import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StepInfoComponent } from './step-info/step-info.component';
import { StepInfoHighlightComponent } from './step-info-highlight/step-info-highlight.component';

@NgModule({
  declarations: [
    AppComponent,
    StepInfoComponent,
    StepInfoHighlightComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
