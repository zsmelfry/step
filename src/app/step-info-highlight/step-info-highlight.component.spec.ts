import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepInfoHighlightComponent } from './step-info-highlight.component';

describe('StepInfoHighlightComponent', () => {
  let component: StepInfoHighlightComponent;
  let fixture: ComponentFixture<StepInfoHighlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepInfoHighlightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepInfoHighlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
