import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-step-info-highlight',
  templateUrl: './step-info-highlight.component.html',
  styleUrls: ['./step-info-highlight.component.scss']
})
export class StepInfoHighlightComponent implements OnInit {

  @Input() icon: string;
  @Input() title: string;
  @Input() number: number;
  @Input() info: string;

  constructor() { }

  ngOnInit(): void {
  }

}
