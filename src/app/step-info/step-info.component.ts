import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-step-info',
  templateUrl: './step-info.component.html',
  styleUrls: ['./step-info.component.scss']
})
export class StepInfoComponent implements OnInit {

  @Input() stepIndex: number;
  @Input() title: string;
  @Input() number: number;
  @Input() info: string;

  constructor() { }

  ngOnInit(): void {
  }

}
